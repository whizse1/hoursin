# hoursin

Simple cumulative time tracker written in Python.

```
usage: hoursin [-h] [-t tag] [-l] [-v] ["exe arg1 arg2"]

positional arguments:
  "exe arg1 arg2"    Executable with optional quoted argument list to run and
                     track time for,

options:
  -h, --help         show this help message and exit
  -t tag, --tag tag  Track time using this tag
  -l, --list-time    Display total time for tag. Lists all tracked times with
                     no argument given.
  -v, --verbose      Print total (and elapsed) time when sessions starts and
                     ends.
```

Sample output
```
$ hoursin -l
'What Remains of Edith Finch'	1.2 hours.
'System Shock'	21.2 hours.
'A New Beginning - Final Cut'	7.4 hours.
```

Time is saved in plain text format in $XDG_DATA_DIRS/hoursin/
```
$ cat What\ Remains\ of\ Edith\ Finch.time 
4260.244966
```

