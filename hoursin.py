#!/usr/bin/python3

import argparse
import subprocess
import shutil
import datetime
import base64
import os
import pathlib

import xdg

APPNAME = "hoursin"
VERBOSE=False
EXT = ".time"

def print_v(text):
    if VERBOSE:
        print(text)

def decimal_hours(seconds):
    # Return seconds for secononds < 60
    # Minutes for seconds > 60 < 1800.
    return seconds / 3600

def name_to_filename(name):
    return(name + EXT)

def filename_to_name(filename):
    return(filename.replace(EXT, ""))

def get_data_dir():
    return os.path.join(xdg.xdg_data_home(), APPNAME)

def get_full_path(filename):
    return os.path.join(get_data_dir(), filename)

def parse_timefile(timefile):
    return float(timefile)

def create_timefile(name, seconds):
    return ("%s\n" % (seconds))

def load_timefile(name):
    filename = name_to_filename(name)
    try:
        with open(get_full_path(filename), "rt") as tf:
            timefile = tf.read()
            return parse_timefile(timefile)
    except Exception as e:
        # print(e)
        return 0

def save_timefile(name, seconds):
    filename = name_to_filename(name)
    pathlib.Path(get_data_dir()).mkdir(parents=True, exist_ok=True)

    try:
        with open(get_full_path(filename), "wt") as tf:
            timefile = create_timefile(name, seconds)
            tf.write(timefile)
    except Exception as e:
        print(e)
        return False

parser = argparse.ArgumentParser(description='Simple cumulative time tracker.')
parser.add_argument('exe', metavar='exe "arg1 arg2"', nargs='?', help='Executable with optional quoted argument list to run and track time for,', default=None)
parser.add_argument('-t', '--tag', dest='tag', metavar='tag', help='Track time using this tag', default=None)
parser.add_argument('-l', '--list-time', dest='display', action='store_true',
                    help='Display total time for tag. Lists all tracked times with no argument given.')
parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    help='Print total (and elapsed) time when sessions starts and ends.')

args = parser.parse_args()

if args.verbose:
    VERBOSE = True

if args.display:
    if not args.tag:
        for f in os.listdir(get_data_dir()):
            tag = filename_to_name(os.path.basename(f))
            tag = filename_to_name(f)
            try:
                with open(os.path.join(get_data_dir(), f), "rt") as tf:
                    timefile = tf.read()
                duration = parse_timefile(timefile)
                print("'%s'\t%.1f hours." % (tag, decimal_hours(duration)))
            except Exception as e:
                print("'%s': error reading time record (%s)" % (tag, str(e)))

        exit(0)
    else:
        duration = load_timefile(args.tag)
        if duration == 0:
            print("%s: no time record found." % (args.tag))
        else:
            print("'%s'\t%.1f hours." % (args.tag, decimal_hours(duration)))
    exit(0)

if not args.exe:
    print("Error: the following arguments are required: exe") # Print help instead?
    exit(1)

if not args.tag:
    print("Error: the following arguments are required: tag name")
    exit(1)

try:   
    old_duration = load_timefile(args.tag)
    if old_duration == 0:
        print_v("Tracking time as '%s'." % (args.tag))
    else:
        print_v("Resume tracking time for '%s' (total %.1f hours)." % (args.tag, decimal_hours(old_duration)))
    start = datetime.datetime.now()
    subprocess.run([args.exe], shell=True)
    current_duration = (datetime.datetime.now() - start).total_seconds()
    total_duration = old_duration + current_duration
    print_v("'%s' session lasted %.1f hours (total %.1f hours)." % (args.tag, decimal_hours(current_duration), decimal_hours(total_duration)))
    save_timefile(args.tag, total_duration)
except Exception as e:
    print(e)
    exit(1)
